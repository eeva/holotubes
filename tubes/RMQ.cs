﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tubes
{
    class RMQ
    {
        public string datas = "";
        public bool success = false;
        public HelloWorldApplication refHolo;

        public ConnectionFactory connectionFactory;
        public IConnection connection;
        public IModel channel;
        private EventingBasicConsumer consumer;

        public RMQ(HelloWorldApplication holo)
        {
            this.refHolo = holo;
        }

        public void InitRMQConnection(string host = "167.205.7.226", int port = 5672, string user = "ARmachine",
        string pass = "12345", string vhost = "/ARX")
        {
            connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = host;
            connectionFactory.Port = port;
            connectionFactory.UserName = user;
            connectionFactory.Password = pass;
            connectionFactory.VirtualHost = vhost;
        }
        public void CreateRMQConnection()
        {
            connection = connectionFactory.CreateConnection();
            Debug.WriteLine("Koneksi " + (connection.IsOpen ? "Berhasil! " : "Gagal! "));
        }
        public void CreateRMQChannel(string queue_name, string routingKey = "TMDG2017-5-Ifadin", string exchange_name = "TMDG2017-5-Ifadin")
        {
            if (connection.IsOpen)
            {
                channel = connection.CreateModel();
                Debug.WriteLine("Channel " + (channel.IsOpen ? "Berhasil! " : "Gagal! "));
            }
            if (channel.IsOpen)
            {
                channel.QueueDeclare(queue: queue_name,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
                Debug.WriteLine("Queue telah dideklarasikan. . ");
                consumer = new EventingBasicConsumer(channel);
            }
        }
        public void SendMessage(string tujuan, string msg = "send")
        {
            byte[] responseBytes = Encoding.UTF8.GetBytes(msg); // konversi pesan dalam bentuk stringmenj adi byte
            channel.BasicPublish(exchange: "",
            routingKey: tujuan,
            basicProperties: null,
            body: responseBytes);
            Debug.WriteLine("Pesan: ' " + msg + "' telah dikirim. ");
        }
        public void WaitingMessage(string queue_name)
        {
            
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                datas = message;

                refHolo.showData();
            };

            channel.BasicConsume(queue: queue_name,
            noAck: true,
            consumer: consumer);
        }

        public string getData()
        {
            return datas;
        }
        public void Disconnect()
        {
            channel.Close();
            channel = null;
            Debug.WriteLine("Channel ditutup! ");
            if (connection.IsOpen)
            {
                connection.Close();
            }
            Debug.WriteLine("Koneksi diputus! ");
            connection.Dispose();
            connection = null;
        }
    }
}
