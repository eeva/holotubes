﻿using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Urho;
using Urho.Actions;
using Urho.SharpReality;
using Urho.Shapes;
using Urho.Resources;
using Urho.Gui;
using System.Diagnostics;

namespace tubes
{
    internal class Program
    {
        [MTAThread]
        static void Main()
        {
            var appViewSource = new UrhoAppViewSource<HelloWorldApplication>(new ApplicationOptions("Data"));
            appViewSource.UrhoAppViewCreated += OnViewCreated;
            CoreApplication.Run(appViewSource);
        }

        static void OnViewCreated(UrhoAppView view)
        {
            view.WindowIsSet += View_WindowIsSet;
        }

        static void View_WindowIsSet(Windows.UI.Core.CoreWindow coreWindow)
        {
            // you can subscribe to CoreWindow events here
        }
    }

    public class HelloWorldApplication : StereoApplication
    {
        public Node boardNode, pion2Node, pion1Node, newPion2Node, diceNode, winloseNode;
        public Node[,] pionNode = new Node[5,5];
        Text textElement;
        double x = 0.065;
        double z = 0.065;
        float initx = -0.39f;
        float inity = 1.5f;
        float initz = 0.065f;
        int step, step2, step_awal, step2_awal;
        bool selesai;
        int player;
        bool adadice, bisa_jalan, bisa_jalan_2, satuputaran;
        float addition = 0.065f;

        RMQ rabbitmq;
        RMQ rabbitmq2;

        public HelloWorldApplication(ApplicationOptions opts) : base(opts) { }

        protected override async void Start()
        {
            // Create a basic scene, see StereoApplication
            base.Start();

            player = 1;
            adadice = false;
            bisa_jalan = false;
            bisa_jalan_2 = false;
            satuputaran = false;

            rabbitmq = new RMQ(this);
            rabbitmq.InitRMQConnection();
            rabbitmq.CreateRMQConnection();
            rabbitmq.CreateRMQChannel("TMDG2017-5-Ifadin");

            rabbitmq2 = new RMQ(this);
            rabbitmq2.InitRMQConnection();
            rabbitmq2.CreateRMQConnection();
            rabbitmq2.CreateRMQChannel("TMDG2017-3-Ifadin");

            // Enable input
            EnableGestureManipulation = true;
            EnableGestureTapped = true;

            // Create a node for the Board
            boardNode = Scene.CreateChild();
            boardNode.Position = new Vector3(0, -1f, 2.2f); 
            boardNode.SetScale(0.5f); //D=30cm
            boardNode.Scale = new Vector3(1f, 0.01f, 1f);


            // Scene has a lot of pre-configured components, such as Cameras (eyes), Lights, etc.
            DirectionalLight.Brightness = 1f;
            DirectionalLight.Node.SetDirection(new Vector3(-1, 0, 0.5f));

            //Sphere is just a StaticModel component with Sphere.mdl as a Model.
            var board = boardNode.CreateComponent<Box>();
            board.Material = Material.FromImage("Textures/ludo.jpg");
            
            CreatePion(1);
            CreatePion(2);
            await RegisterCortanaCommands(new Dictionary<string, Action> {
                //{ "big", () => RollTheDice() }
                });
            rabbitmq.WaitingMessage("TMDG2017-5-Ifadin");
            showData();

        }

        public void CreateDice(int dicerandom)
        {
            diceNode = boardNode.CreateChild();
            diceNode.Position = new Vector3(0f, 1.5f, -0.65f);
            diceNode.SetScale(0.2f);
            var dice = diceNode.CreateComponent<Box>();
            Debug.WriteLine("DADU = " + dicerandom);
            switch (dicerandom)
            {
                case 1:
                    dice.Material = Material.FromImage("Textures/1.png");
                    break;
                case 2:
                    dice.Material = Material.FromImage("Textures/2.png");
                    break;
                case 3:
                    dice.Material = Material.FromImage("Textures/3.png");
                    break;
                case 4:
                    dice.Material = Material.FromImage("Textures/4.png");
                    break;
                case 5:
                    dice.Material = Material.FromImage("Textures/5.png");
                    break;
                case 6:
                    dice.Material = Material.FromImage("Textures/6.png");
                    if (bisa_jalan == false)
                    {
                        step = 1;
                        pionNode[1, 1].Position = new Vector3(-0.39f, 1.5f, 0.065f); //maju
                    }
                    break;
            }
            adadice = true;
        }

        public void showData()
        {
            Debug.WriteLine("data shown: " + rabbitmq.datas);
            int dicerandom2;
            bool success = int.TryParse(rabbitmq.datas, out dicerandom2);
            //////////////////////////////////////////////
            Debug.WriteLine("DICE ROLL FOR PLAYER 2");

            diceNode = boardNode.CreateChild();
            diceNode.Position = new Vector3(0f, 1.5f, -0.65f);
            diceNode.SetScale(0.2f);
            var dice = diceNode.CreateComponent<Box>();
            Debug.WriteLine("DADU P2 = " + dicerandom2);
            switch (dicerandom2)
            {
                case 1:
                    dice.Material = Material.FromImage("Textures/1.png");
                    break;
                case 2:
                    dice.Material = Material.FromImage("Textures/2.png");
                    break;
                case 3:
                    dice.Material = Material.FromImage("Textures/3.png");
                    break;
                case 4:
                    dice.Material = Material.FromImage("Textures/4.png");
                    break;
                case 5:
                    dice.Material = Material.FromImage("Textures/5.png");
                    break;
                case 6:
                    dice.Material = Material.FromImage("Textures/6.png");
                    if (bisa_jalan_2 == false)
                    {
                        step2 = 14;
                        pionNode[2, 1].Position = new Vector3(0.065f, 1.5f, 0.39f); //maju
                    }
                    break;
            }
            step2_awal = step;
            //Debug.WriteLine("step = " + step);
            //Debug.WriteLine("bisajalan2 = " + bisa_jalan_2);
            Debug.WriteLine("--==step awal = " + step2);
            bisa_jalan_2 = GerakinPion2(2, 1, dicerandom2, bisa_jalan_2);
            Debug.WriteLine("--==step awal = " + step2);
        }

        public void CreatePion(int player) {
            for (int i = 1; i <= 4; i++) {
                pionNode[player, i] = boardNode.CreateChild();
                pionNode[player, i].SetScale(0.05f); //27% of the Earth's size
                pionNode[player, i].Scale = new Vector3(0.05f, 5f, 0.05f);
                switch (player) {
                    case 1: //player 1
                        switch (i)
                        {
                            case 1:
                                //pionNode[player, i].Position = new Vector3(-0.39f, 1.5f, 0.065f); //maju
                                pionNode[player, i].Position = new Vector3(-0.2925f, 1.5f, 0.2f); //home semua
                                break;
                            case 2:
                                pionNode[player, i].Position = new Vector3(-0.3575f, 1.5f, 0.2925f);
                                break;
                            case 3:
                                pionNode[player, i].Position = new Vector3(-0.2925f, 1.5f, 0.3575f);
                                break;
                            case 4:
                                pionNode[player, i].Position = new Vector3(-0.2275f, 1.5f, 0.2925f);
                                break;
                        }
                        break;
                    case 2: //player 2
                        switch (i)
                        {
                            case 1:
                                //pionNode[player, i].Position = new Vector3(0.065f, 1.5f, 0.39f); //maju
                                pionNode[player, i].Position = new Vector3(0.2925f, 1.5f, 0.2f); //home semua
                                break;
                            case 2:
                                pionNode[player, i].Position = new Vector3(0.3575f, 1.5f, 0.2925f);
                                break;
                            case 3:
                                pionNode[player, i].Position = new Vector3(0.2925f, 1.5f, 0.3575f);
                                break;
                            case 4:
                                pionNode[player, i].Position = new Vector3(0.2275f, 1.5f, 0.2925f);
                                break;
                        }
                        break;
                }
                var pion = pionNode[player, i].CreateComponent<StaticModel>();
                pion.Model = CoreAssets.Models.Box;
                pion.SetMaterial(Material.FromImage("Textures/p"+player+"_"+i+".png"));
            }            
        }
        
        public bool GerakinPion(int player, int pionke, int dicerandom, bool bisajalan) {
            Debug.WriteLine("awal bisajalan =" + bisajalan);
            
            string arah = "diam";
            
            if (bisajalan == false && step == 0 && dicerandom != 6) {
                arah = "diam";
            }
            else if(((player == 1 && step == 1) || (player == 2 && step == 14)) && (dicerandom == 6 && bisajalan == false))
            {
                bisajalan = true;
                if(player == 1)
                    step = 1;
                else if(player == 2)
                    step = 14;
            }
            else if (bisajalan == true && step >= 1)
            {
                for (int i = 1; i <= dicerandom; i++)
                {
                    Debug.WriteLine("satuputaran = " + satuputaran);
                    // KALO UDAH SATU PUTARAN, PLAYER 1 KEKANAN
                    if (player == 1 && step == 51)
                    {
                        Debug.WriteLine("kond1");
                        arah = "kanan";
                        Debug.WriteLine("step 1= " + step);
                        satuputaran = true;
                    }
                    //kalo bisa ke finish
                    else if (player == 1 && satuputaran == true && step < 5 && dicerandom <= (5 - step_awal))
                    {
                        Debug.WriteLine("kond2");
                        arah = "kanan";
                        Debug.WriteLine("step 2= " + step);
                    }
                    //kalo gabisa ke finish (mantul)
                    else if (player == 1 && satuputaran == true && step < 5 && dicerandom > (5 - step_awal))
                    {
                        Debug.WriteLine("kond3");
                        arah = "diam";
                    }
                    //kalo sudah finish
                    else if (player == 1 && satuputaran == true && step == 5)
                    {
                        WinLose("win");
                        Debug.WriteLine("MENANG");
                    }

                    ////KALO UDAH SATU PUTARAN, PLAYER 2 KEBAWAH
                    //else if (player == 2 && step == 12)
                    //{
                    //    arah = "bawah";
                    //    satuputaran = true;
                    //}
                    ////kalo bisa ke finish
                    //else if (player == 2 && satuputaran == true && step < 5 && dicerandom <= (5 - step_awal))
                    //{
                    //    arah = "kanan";
                    //}
                    ////kalo gabisa ke finish (mantul)
                    //else if (player == 2 && satuputaran == true && step < 5 && dicerandom > (5 - step_awal))
                    //{
                    //    arah = "diam";
                    //}
                    ////kalo sudah finish
                    //else if (player == 2 && satuputaran == true && step2 == 5 )
                    //{  
                    //    WinLose("lose");
                    //}

                    //KALO BELUM 1 PUTARAN
                    else if (satuputaran == false && (step >= 1 && step < 5) || (step >= 11 && step < 13) || (step >= 19 && step < 24) || (step == 52)) //|| (step == 51)
                    {
                        arah = "kanan";
                    }
                    else if (satuputaran == false && step == 5)
                    {
                        arah = "kanan_atas";
                    }
                    else if (satuputaran == false && (step >= 6 && step < 11) || (step >= 39 && step < 44) || (step >= 50 && step < 52))
                    {
                        arah = "atas";
                    }
                    else if (satuputaran == false && (step >= 13 && step < 18) || (step >= 13 && step < 18) || (step >= 24 && step < 26) || (step >= 32 && step < 37))
                    {
                        arah = "bawah";
                    }
                    else if (satuputaran == false && step == 18)
                    {
                        arah = "kanan_bawah";
                    }
                    else if (satuputaran == false && (step >= 26 && step < 31) || (step >= 37 && step < 39) || (step >= 45 && step < 51))
                    {
                        arah = "kiri";
                    }
                    else if (satuputaran == false && step == 31)
                    {
                        arah = "kiri_bawah";
                    }
                    else if (satuputaran == false && step == 44)
                    {
                        arah = "kiri_atas";
                    }
                    
                    switch (arah)
                    {
                        case "kanan":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X + addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z);
                            Debug.WriteLine("kanan");
                            break;
                        case "kiri":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X - addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z);
                            Debug.WriteLine("kiri");
                            break;
                        case "atas":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z + addition);
                            Debug.WriteLine("atas");
                            break;
                        case "bawah":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z - addition);
                            Debug.WriteLine("bawah");
                            break;
                        case "kanan_atas":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X + addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z + addition);
                            Debug.WriteLine("serong kanan atas");
                            break;
                        case "kanan_bawah":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X + addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z - addition);
                            Debug.WriteLine("serong kanan bawah");
                            break;
                        case "kiri_atas":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X - addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z + addition);
                            Debug.WriteLine("serong kiri atas");
                            break;
                        case "kiri_bawah":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X - addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z - addition);
                            Debug.WriteLine("serong kiri bawah");
                            break;
                    }
                    ////TAMBAH STEP
                    if (arah != "diam") {
                        if (step < 52)
                        {
                            step += 1;
                        }
                        else
                        {
                            step = 1;
                        }
                    }
                    //kalo sudah finish
                    if (player == 1 && satuputaran == true && step == 5)
                    {
                        WinLose("win");
                        Debug.WriteLine("MENANG");
                    }
                }
            }
            else {

            }
            Debug.WriteLine("akhir bisajalan =" + bisajalan);
            return bisajalan; 
        }

        public bool GerakinPion2(int player, int pionke, int dicerandom, bool bisajalan)
        {
            Debug.WriteLine("awal bisajalan =" + bisajalan);
            step2_awal = step2;
            bool mundur = false;
            string arah = "diam";

            if (bisajalan == false && step2 == 0 && dicerandom != 6)
            {
                arah = "diam";
            }
            else if (((player == 1 && step2 == 1) || (player == 2 && step2 == 14)) && (dicerandom == 6 && bisajalan == false))
            {
                bisajalan = true;
                if (player == 1)
                    step2 = 1;
                else if (player == 2)
                    step2 = 14;
            }
            else if (bisajalan == true && step2 >= 1)
            {
                for (int i = 1; i <= dicerandom; i++)
                {
                    //Debug.WriteLine("satuputaran = " + satuputaran);
                    // KALO UDAH SATU PUTARAN, PLAYER 1 KEKANAN
                    //Debug.WriteLine("player = " + player);
                    //if (player == 1 && step2 == 51)
                    //{
                    //    arah = "kanan";
                    //    Debug.WriteLine("step2 1= " + step2);
                    //    satuputaran = true;
                    //}
                    ////kalo bisa ke finish
                    //else if (player == 1 && satuputaran == true && step2 < 5 && dicerandom <= (5 - step2_awal))
                    //{
                    //    arah = "kanan";
                    //    Debug.WriteLine("step2 2= " + step2);
                    //}
                    ////kalo gabisa ke finish (mantul)
                    //else if (player == 1 && satuputaran == true && step2 < 5 && dicerandom > (5 - step2_awal))
                    //{
                    //    arah = "diam";
                    //}
                    ////kalo sudah finish
                    //else if (player == 1 && satuputaran == true && step2 == 5)
                    //{
                    //    WinLose("win");
                    //}

                    //KALO UDAH SATU PUTARAN, PLAYER 2 KEBAWAH
                    if (player == 2 && step2 == 12)
                    {
                        arah = "bawah";
                        satuputaran = true;
                        Debug.WriteLine("kond1");
                    }
                    //kalo bisa ke finish
                    else if (player == 2 && satuputaran == true && step2 < 18 && dicerandom <= (18 - step2_awal))
                    {
                        arah = "bawah";
                        Debug.WriteLine("kond2");
                        Debug.WriteLine("18-"+step2_awal+"="+ (18 - step2_awal));
                    }
                    //kalo gabisa ke finish (mantul)
                    else if (player == 2 && satuputaran == true && step2 < 18 && dicerandom > (18 - step2_awal))
                    {
                        arah = "diam";
                        Debug.WriteLine("kond3");
                        Debug.WriteLine("18-" + step2_awal + "=" + (18 - step2_awal));
                    }
                    
                    //KALO BELUM 1 PUTARAN
                    else if (satuputaran == false && (step2 >= 1 && step2 < 5) || (step2 >= 11 && step2 < 13) || (step2 >= 19 && step2 < 24) || (step2 == 52)) //|| (step2 == 51)
                    {
                        arah = "kanan";
                    }
                    else if (satuputaran == false && step2 == 5)
                    {
                        arah = "kanan_atas";
                    }
                    else if (satuputaran == false && (step2 >= 6 && step2 < 11) || (step2 >= 39 && step2 < 44) || (step2 >= 50 && step2 < 52))
                    {
                        arah = "atas";
                    }
                    else if (satuputaran == false && (step2 >= 13 && step2 < 18) || (step2 >= 13 && step2 < 18) || (step2 >= 24 && step2 < 26) || (step2 >= 32 && step2 < 37))
                    {
                        arah = "bawah";
                    }
                    else if (satuputaran == false && step2 == 18)
                    {
                        arah = "kanan_bawah";
                    }
                    else if (satuputaran == false && (step2 >= 26 && step2 < 31) || (step2 >= 37 && step2 < 39) || (step2 >= 45 && step2 < 51))
                    {
                        arah = "kiri";
                    }
                    else if (satuputaran == false && step2 == 31)
                    {
                        arah = "kiri_bawah";
                    }
                    else if (satuputaran == false && step2 == 44)
                    {
                        arah = "kiri_atas";
                    }

                    switch (arah)
                    {
                        case "kanan":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X + addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z);
                            Debug.WriteLine("kanan");
                            break;
                        case "kiri":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X - addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z);
                            Debug.WriteLine("kiri");
                            break;
                        case "atas":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z + addition);
                            Debug.WriteLine("atas");
                            break;
                        case "bawah":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z - addition);
                            Debug.WriteLine("bawah");
                            break;
                        case "kanan_atas":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X + addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z + addition);
                            Debug.WriteLine("serong kanan atas");
                            break;
                        case "kanan_bawah":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X + addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z - addition);
                            Debug.WriteLine("serong kanan bawah");
                            break;
                        case "kiri_atas":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X - addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z + addition);
                            Debug.WriteLine("serong kiri atas");
                            break;
                        case "kiri_bawah":
                            pionNode[player, 1].Position = new Vector3(pionNode[player, 1].Position.X - addition, pionNode[player, 1].Position.Y, pionNode[player, 1].Position.Z - addition);
                            Debug.WriteLine("serong kiri bawah");
                            break;
                    }
                    ////TAMBAH step2
                    if (arah != "diam")
                    {
                        if (step2 < 52)
                        {
                            step2 += 1;
                        }
                        else
                        {
                            step2 = 1;
                        }
                    }

                    //kalo sudah finish
                    if (player == 2 && satuputaran == true && step2 == 18)
                    {
                        WinLose("lose");
                    }
                }
            }
            else
            {

            }
            Debug.WriteLine("akhir bisajalan =" + bisajalan);
            return bisajalan;
        }

        public void WinLose(string win) {
            winloseNode = boardNode.CreateChild();
            winloseNode.Position = new Vector3(0f, 2f, -0.65f);
            winloseNode.SetScale(0.2f);
            var wl = winloseNode.CreateComponent<Box>();
            if (win == "win") {
                wl.Material = Material.FromImage("Textures/win.png");
                Debug.WriteLine("!!!!ANDA MENANG!!!!");
            }else if (win == "lose")
            {
                wl.Material = Material.FromImage("Textures/lose.png");
                Debug.WriteLine("!!!!ANDA KALAH!!!!");
            }
        }

        public override void OnGestureTapped()
        {
            Debug.WriteLine("ROLLED PLAYER 1");
            Random rnd = new Random();
            int dicerandom = rnd.Next(1, 7);
            
            if (adadice) {
                diceNode.Remove();
            }
            
            CreateDice(dicerandom);
            step_awal = step;
            Debug.WriteLine("--==step awal = "+step);

            bisa_jalan = GerakinPion(1, 1, dicerandom, bisa_jalan);
            
            Debug.WriteLine("--==step akhir = " + step);

            rabbitmq2.SendMessage("TMDG2017-3-Ifadin", dicerandom.ToString());
        }
        public override void OnGestureDoubleTapped()
        {
        }
    }
}